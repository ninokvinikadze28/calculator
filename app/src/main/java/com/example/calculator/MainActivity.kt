package com.example.calculator

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.example.calculator.R
import kotlinx.android.synthetic.main.activity_main.*
import org.w3c.dom.Text

@Suppress("UNSAFE_CALL_ON_PARTIALLY_DEFINED_RESOURCE")
class MainActivity : AppCompatActivity(), View.OnClickListener {
    private var firstVariable = 0.0
    private var secondVariable = 0.0
    private var operation = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        button0.setOnClickListener(this)
        button1.setOnClickListener(this)
        button2.setOnClickListener(this)
        button3.setOnClickListener(this)
        button4.setOnClickListener(this)
        button5.setOnClickListener(this)
        button6.setOnClickListener(this)
        button7.setOnClickListener(this)
        button8.setOnClickListener(this)
        button9.setOnClickListener(this)
        deleteButton.setOnLongClickListener {
            resultTextView.text = ""
            return@setOnLongClickListener true
        }



    }
    @SuppressLint("SetTextI18n")
    fun fullStop(View: View){
        fullStopButton.isClickable = true
        var result = resultTextView.text.toString()
        resultTextView.text = "$result."

        if(resultTextView.text.contains(".")){
            fullStopButton.isClickable = false
        }

    }

    fun delete(View: View) {
        val value = resultTextView.text.toString()
        if (value.isNotEmpty()) {
            resultTextView.text = value.substring(0, value.length - 1)


        }
        deleteButton.setOnLongClickListener {
            true
            resultTextView.text.toString().isEmpty()
        }


    }

    fun equal(View: View) {
        val value = resultTextView.text.toString()
        var result = 0.0
        if (value.isNotEmpty()) {
            secondVariable = value.toInt().toDouble()

            if (operation == ":") {
                result = firstVariable / secondVariable

            } else if (operation == "+") {
                result = firstVariable + secondVariable

            } else if (operation == "-"){
                result = firstVariable - secondVariable

            } else if (operation == "x")  {
                result = firstVariable * secondVariable
            }
            resultTextView.text = result.toString()

        }
    }
    fun division(View: View) {
        val value = resultTextView.text.toString()

        operation = ":"
        if (value.isNotEmpty() && secondVariable != 0.0) {
            firstVariable = value.toInt().toDouble()
            resultTextView.text = ""
        }else if(value.isNotEmpty() && secondVariable.toInt() == 0){
            operation = ":"
            firstVariable = value.toInt().toDouble()
            resultTextView.text = ""
            Toast.makeText(this, "Division by Zero", Toast.LENGTH_SHORT).show()
        }

    }

    fun addition(View: View) {
        val value = resultTextView.text.toString()
        operation = "+"
        firstVariable = value.toInt().toDouble()
        resultTextView.text = ""


    }

    fun substraction(View: View) {
        val value = resultTextView.text.toString()
        operation = "-"
        firstVariable = value.toInt().toDouble()
        resultTextView.text = ""


    }

    fun multiplication(View: View) {
        val value = resultTextView.text.toString()
        operation = "x"
        firstVariable = value.toInt().toDouble()
        resultTextView.text = ""


    }



    @SuppressLint("SetTextI18n")
    override fun onClick(v: View?) {
        val button = v as Button
        resultTextView.text = resultTextView.text.toString() + button.text.toString()


    }


}





